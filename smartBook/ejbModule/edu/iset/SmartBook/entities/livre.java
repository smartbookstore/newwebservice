package edu.iset.SmartBook.entities;

import java.io.Serializable;
import java.sql.Blob;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class livre implements Serializable{
	

	
	@Id
	private int cin;
	private String titre;
	private String auteur;
	private String rebrique;
	private int stock;
	private Blob image;

	
	public Blob getImage() {
		return image;
	}

	public void setImage(Blob image) {
		this.image = image;
	}

	public int getCin() {
		return cin;
	}

	public void setCin(int cin) {
		this.cin = cin;
	}

	public livre() {
		
	}
	@Override
	public String toString() {
		return "livre [cin=" + cin + ", titre=" + titre + ", auteur=" + auteur + ", rebrique=" + rebrique + ", stock="
				+ stock + "]";
	}
	
	public String getTitre() {
		return titre;
	}
	public void setTitre(String titre) {
		this.titre = titre;
	}
	public String getAuteur() {
		return auteur;
	}
	public void setAuteur(String auteur) {
		this.auteur = auteur;
	}
	public String getRebrique() {
		return rebrique;
	}
	public void setRebrique(String rebrique) {
		this.rebrique = rebrique;
	}
	public int getStock() {
		return stock;
	}
	public void setStock(int stock) {
		this.stock = stock;
	}
	public livre(int cin, String titre, String auteur, String rebrique, int stock) {
		super();
		this.cin = cin;
		this.titre = titre;
		this.auteur = auteur;
		this.rebrique = rebrique;
		this.stock = stock;
	}
	
	
	
	

}

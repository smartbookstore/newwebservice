package edu.iset.SmartBook.entities;
import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;


@Entity
public class utilisateur implements Serializable{
	@Id
	private int cin;
	private String nom;
	private String prenom;
	private String adresse;
	private String motPasse;
	private String role;
	public int getCin() {
		return cin;
	}
	public utilisateur() {}

	public void setCin(int cin) {
		this.cin = cin;
	}


	public String getNom() {
		return nom;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}
	public String getPrenom() {
		return prenom;
	}
	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}
	public String getAdresse() {
		return  adresse;
	}
	public void setAdresse(String  adresse) {
		this. adresse =  adresse;
	}
	public String getMotPasse() {
		return motPasse;
	}
	public void setMotPasse(String motPasse) {
		this.motPasse = motPasse;
	}
	public String getRole() {
		return role;
	}
	public void setRole(String role) {
		this.role = role;
	}


	@Override
	public String toString() {
		return "utilisateur [cin=" + cin + ", nom=" + nom + ", prenom=" + prenom + ", adresse=" +  adresse
				+ ", motPasse=" + motPasse + ", Role=" + role + "]";
	}


	public utilisateur(int cin, String nom, String prenom, String adresse, String motPasse, String role) {
		super();
		this.cin = cin;
		this.nom = nom;
		this.prenom = prenom;
		this.adresse=adresse;
		this.motPasse = motPasse;
		this.role = role;
	}
	
	
	
	
		

}

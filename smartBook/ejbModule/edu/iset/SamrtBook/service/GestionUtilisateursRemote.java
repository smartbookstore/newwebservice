package edu.iset.SamrtBook.service;

import java.util.List;

import javax.ejb.Remote;
import javax.ws.rs.PathParam;

import edu.iset.SmartBook.entities.utilisateur;

@Remote
public interface GestionUtilisateursRemote {
	 public utilisateur login(String adresse, String motPasse);
	 public void addd(utilisateur u);
	   public List<utilisateur> findAlluser();
	   public utilisateur finduserByCin(int c);
	   public void deleteUser(int c);
	 
	   public utilisateur modifier(utilisateur u);

}

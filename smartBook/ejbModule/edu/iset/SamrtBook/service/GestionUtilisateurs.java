package edu.iset.SamrtBook.service;

import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import javax.persistence.Query;

import edu.iset.SmartBook.entities.utilisateur;


@Stateless
@LocalBean

	public class GestionUtilisateurs implements GestionUtilisateursRemote, GestionUtilisateursLocal {
		 @PersistenceContext( unitName="smartBook")
	      private EntityManager em;
		 
		 @Override
		   public void addd(utilisateur u) {
			   em.persist(u);
		   }
	   @Override
		 public utilisateur login(String adresse, String motPasse) {
			 
		   try {
				Query req=em.createQuery("select u from utilisateur u where u.adresse=:a AND u.motPasse=:p");
						req.setParameter("a",adresse);
						req.setParameter("p",motPasse);
						return  (utilisateur) req.getSingleResult();
				}
				catch (Exception e)
				{
					return null;
				} 
		 }
	   public List<utilisateur> findAlluser() {
			Query q = em.createQuery("from utilisateur u", utilisateur.class);
			return (q.getResultList());
		}
	   public utilisateur finduserByCin(int c) {
			return (em.find(utilisateur.class, c));

		}

	   public void deleteUser(int c) {
			em.remove(em.merge(finduserByCin( c)));
		}
	   
	   public utilisateur modifier(utilisateur u) {
			return em.merge(u);
		}
	   
		public GestionUtilisateurs() {
	        
	    }
	   
		
	}
   



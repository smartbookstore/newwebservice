package edu.iset.SamrtBook.service;

import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import edu.iset.SmartBook.entities.livre;
import edu.iset.SmartBook.entities.utilisateur;




@Stateless
@LocalBean
public class GestionBook implements GestionBookRemote, GestionBookLocal {
	@PersistenceContext( unitName="smartBook")
    private EntityManager em;
	 
	 @Override
	   public void addd(livre l) {
		   em.persist(l);
	   }
	 public livre findlivreByCin(int c) {
			return em.find(livre.class,c);

		}
	
		   

@Override
public void deleteliv(livre l)
{
	em.remove(em.merge(l));
}
	   
	   public List<livre> findAlllivre() {
			Query q = em.createQuery("from livre l", livre.class);
			return (q.getResultList());
		}
	   
	   public livre modifier(livre l) {
			return em.merge(l);
		}
    
    public GestionBook() {
        
    }
	

	

}

package edu.iset.SamrtBook.service;

import java.util.List;

import javax.ejb.Local;

import edu.iset.SmartBook.entities.utilisateur;

@Local
public interface GestionUtilisateursLocal {
	 public utilisateur login(String adresse, String motPasse);
	 public void addd(utilisateur u);
	   public List<utilisateur> findAlluser();
	   public utilisateur finduserByCin(int c);
	   public void deleteUser(int c);
	   public utilisateur modifier(utilisateur u);
}

package edu.iset.tn;


import java.util.List;

import javax.ejb.EJB;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import edu.iset.SamrtBook.service.GestionBookRemote;
import edu.iset.SamrtBook.service.GestionUtilisateursRemote;
import edu.iset.SmartBook.entities.livre;
import edu.iset.SmartBook.entities.utilisateur;




@Path(value="Book")
public class livree {

	@EJB
	private GestionBookRemote book;
	
	 @POST
		@Produces(MediaType.APPLICATION_JSON)
		@Consumes(MediaType.APPLICATION_JSON)
		@Path(value="/")
		public Response addEtud(@PathParam(value="cin") Integer cin ,livre l){
			 book.addd(l);
			 return Response.status(Status.ACCEPTED).entity("Livre ajouter avec success !!").build();
			
		}
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path(value="/")
	public List<livre> getAllbook(){
		return book.findAlllivre();
	}
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path(value="/{cin}")
	public livre getBookBycin(@PathParam(value="cin") Integer cin){
		return book.findlivreByCin(cin);
	}
	
	
	
	@DELETE
	@Path(value="liv/{c}")
	@Produces(MediaType.APPLICATION_JSON)
	public  void remove1(@PathParam(value="c") Integer c, livre l)
	{
		book.deleteliv(l);
		
	}
	

	@DELETE
	@Path(value="/{c}")
	@Produces(MediaType.APPLICATION_JSON)
	public  Response remove(@PathParam(value="c") Integer c)
	{ livre l=book.findlivreByCin(c);
		book.deleteliv(l);
		return Response.status(Status.ACCEPTED).entity("le livre a �t� supprim�").build();
		
	}


@PUT 
@Path(value="")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public List modifierr( livre l){
	 book.modifier(l);
	 return book.findAlllivre();
}

}

package edu.iset.tn;

import java.util.List;

import javax.ejb.EJB;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import edu.iset.SamrtBook.service.GestionUtilisateursRemote;
import edu.iset.SmartBook.entities.utilisateur;



@Path(value="log")
public class login {
	@EJB
	private GestionUtilisateursRemote user;
	 @POST
	 @Produces(MediaType.APPLICATION_JSON)
	 @Consumes(MediaType.APPLICATION_JSON)
	 @Path(value="login")
	  public  Response login(utilisateur u) {
			
		  user.login(u.getAdresse(), u.getMotPasse());
		  if(u==null) {
			  return Response.status(Status.ACCEPTED).entity("failed").build();
		  }
		  else {
		  return Response.status(Status.ACCEPTED).entity(u).build();
		  }
	  }
	 @POST
		@Produces(MediaType.APPLICATION_JSON)
		@Consumes(MediaType.APPLICATION_JSON)
		@Path(value="/")
		public Response addEtud(@PathParam("cin") int cin ,utilisateur u){
			 user.addd(u);
			 return Response.status(Status.ACCEPTED).entity("Utilisateur ajouter avec success !!").build();
			
		}
	 @GET
		@Produces({MediaType.APPLICATION_JSON})
		@Path(value="/")
		public List<utilisateur> getUtilisateurList(){
			return user.findAlluser();
		}
	@GET
	@Path(value="/{cin}")
	public utilisateur getUserBycin(@PathParam(value="cin") int cin){
		return user.finduserByCin(cin);
	}
	
	
@DELETE
@Path("/{id}")
@Produces(MediaType.APPLICATION_JSON)
public void remove(@PathParam(value="id") int id)
{
	 user.deleteUser(id);
	
}


@PUT 
@Path(value="")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public List modifierr( utilisateur u){
	 user.modifier(u);
	return user.findAlluser();
}

}
